const { assert } = require('chai')
const xml2js = require('xml2js')
const report = require('../../src/report')

describe('reports', () => {
  let parser, results

  before(() => {
    parser = new xml2js.Parser()
    results = [{
      id: 1,
      title: 'mock test 1',
      value: true,
      category: 'category 1',
      time: 1.1
    }, {
      id: 2,
      title: 'mock test 1',
      value: true,
      category: 'category 1',
      time: 2.1
    }, {
      id: 3,
      title: 'mock skipped test',
      skipped: true,
      category: 'category 2'
    }, {
      id: 4,
      title: 'mock failed test',
      value: false
    }]
  })

  it('should include the number of tests ran', async () => {
    const xml = report(results)
    const parsed = await parser.parseStringPromise(xml)
    assert.equal(parsed.testsuites.$.tests, '4')
  })

  it('should include the number of skipped tests', async () => {
    const xml = report(results)
    const parsed = await parser.parseStringPromise(xml)
    assert.equal(parsed.testsuites.$.skipped, '1')
  })

  it('should include the number of failed tests', async () => {
    const xml = report(results)
    const parsed = await parser.parseStringPromise(xml)
    assert.equal(parsed.testsuites.$.failures, '1')
  })

  it('should include the number of failed tests', async () => {
    const xml = report(results)
    const parsed = await parser.parseStringPromise(xml)
    assert.equal(parsed.testsuites.$.failures, '1')
  })

  it('should include time taken to run the tests', async () => {
    const xml = report(results, '1.337')
    const parsed = await parser.parseStringPromise(xml)
    assert.equal(parsed.testsuites.$.time, '1.337')
  })

  it('should group testsuites by category', async () => {
    const xml = report(results)
    const parsed = await parser.parseStringPromise(xml)
    assert.equal(parsed.testsuites.testsuite.length, 3)
  })

  it('should include the number of tests in a testsuite', async () => {
    const xml = report(results)
    const parsed = await parser.parseStringPromise(xml)
    const testsuite = parsed.testsuites.testsuite.find(s => s.$.name === 'category 1')
    assert.equal(testsuite.$.tests, '2')
  })

  it('should include the number of skipped tests in a testsuite', async () => {
    const xml = report(results)
    const parsed = await parser.parseStringPromise(xml)
    const testsuite = parsed.testsuites.testsuite.find(s => s.$.name === 'category 2')
    assert.equal(testsuite.$.skipped, '1')
  })

  it('should include the time taken to run a test suite', async () => {
    const xml = report(results)
    const parsed = await parser.parseStringPromise(xml)
    const testsuite = parsed.testsuites.testsuite.find(s => s.$.name === 'category 1')
    assert.equal(testsuite.$.time, '3.2')
  })

  it('should distinguish skipped tests', async () => {
    const xml = report(results)
    const parsed = await parser.parseStringPromise(xml)
    const testsuite = parsed.testsuites.testsuite.find(s => s.$.name === 'category 2')
    assert.equal(testsuite.testcase[0].skipped[0], '')
  })

  it('should distinguish failed tests', async () => {
    const xml = report(results)
    const parsed = await parser.parseStringPromise(xml)
    const testsuite = parsed.testsuites.testsuite.find(s => s.$.name === 'misc')
    assert.equal(testsuite.testcase[0].failure[0], '')
  })
})
