const { assert } = require('chai')
const contentSecurityPolicyCheck = require('../../src/checks/contentSecurityPolicy')

describe('CSP checks', () => {
  it('should check if CSP header present', async () => {
    const response = {
      headers: {
        'content-security-policy': 'foobar'
      }
    }

    const result = await contentSecurityPolicyCheck(response)
    assert.isTrue(result.find(i => i.id === '2001').value)
  })

  it('should check if no CSP header present', async () => {
    const response = {
      headers: {}
    }

    const result = await contentSecurityPolicyCheck(response)
    assert.isFalse(result.find(i => i.id === '2001').value)
  })
})
