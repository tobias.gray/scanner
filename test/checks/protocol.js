const { assert } = require('chai')
const protocolCheck = require('../../src/checks/protocol')

describe('protocol checks', () => {
  const httpResponse = { responseUrl: 'https://example.com' }
  const httpsResponse = {}

  it('should check if endpoint is reachable on HTTPS', async () => {
    const result = await protocolCheck(httpResponse, httpsResponse)
    assert.isTrue(result.find(i => i.id === '1001').value)
    assert.isTrue(result.find(i => i.id === '1002').value)
  })

  it('should check if endpoint is unreachable on HTTPS', async () => {
    const result = await protocolCheck(httpResponse, null)
    assert.isFalse(result.find(i => i.id === '1001').value)
  })

  it('should check if endpoint is unreachable on HTTP', async () => {
    const result = await protocolCheck(null, httpsResponse)
    assert.isFalse(result.find(i => i.id === '1002').value)
  })

  it('should check if HTTP is redirected to HTTPS', async () => {
    const goodResult = await protocolCheck(httpResponse, httpsResponse)
    assert.isTrue(goodResult.find(i => i.id === '1003').value)

    const badResult = await protocolCheck({ responseUrl: 'http://example.com' }, httpsResponse)
    assert.isFalse(badResult.find(i => i.id === '1003').value)
  })
})
