const { create } = require('xmlbuilder2')

/**
 * Given a set of scan results will generate a XML report adhering to the JUnit
 * schema. This means we can use the report in MR widgets.
 *
 * However it is worth noting JUnit reports are built for unit tests with pass &
 * fail scenarios in mind and not things like severity or other attributes so
 * some fidelity is lost.
 *
 * @param {Array} results
 */
module.exports = (results, time) => {
  const report = create({ version: '1.0', encoding: 'UTF-8' })

  const tests = results.length
  const failures = results.filter(r => r.skipped !== true && r.value === false).length
  const skipped = results.filter(r => r.skipped === true).length

  const testsuites = report.ele('testsuites', {
    name: 'Scan Results',
    time,
    tests,
    failures,
    skipped
  })

  // go through the tests and group them by category/suite
  const suites = {}
  for (let i = 0; i < results.length; i++) {
    const { id, title, value, skipped, time, severity = 'low', category = 'misc' } = results[i]
    const testcase = { name: `${id} | ${severity} | ${title}`, classname: category, time, value, skipped }

    if (suites[category]) {
      suites[category].push(testcase)
    } else {
      suites[category] = [testcase]
    }
  }

  // generate report suites and testcases
  for (const [key, testcases] of Object.entries(suites)) {
    const tests = testcases.length
    const failures = testcases.filter(r => r.skipped !== true && r.value === false).length
    const skipped = testcases.filter(r => r.skipped === true).length
    const time = testcases.reduce((a, b) => {
      return a + (b.time || 0)
    }, 0)

    const suite = testsuites
      .ele('testsuite', { name: key, tests, time, failures, skipped })

    for (let i = 0; i < testcases.length; i++) {
      const { name, classname, category, time, value, skipped } = testcases[i]
      const testcase = suite.ele('testcase', { name, classname, category, time })

      if (skipped) {
        testcase.ele('skipped')
      } else if (!value) {
        testcase.ele('failure')
      }
    }
  }

  return report.end({ prettyPrint: true })
}
