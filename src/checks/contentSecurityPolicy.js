module.exports = async (response) => {
  const checks = []

  checks.push(checkCSPHeader(response))

  return checks
}

const checkCSPHeader = (response) => {
  const start = new Date()
  const value = Boolean(response.headers['content-security-policy'])
  const end = new Date()
  const time = (end.getTime() - start.getTime()) / 1000

  return {
    id: '2001',
    title: 'Content Security Policy header present',
    value,
    severity: 'medium',
    category: 'Headers',
    time
  }
}
