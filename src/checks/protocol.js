module.exports = async (httpResponse, httpsResponse) => {
  const checks = []

  checks.push(reachableWithHttps(httpsResponse))
  checks.push(reachableWithHttp(httpResponse))
  checks.push(httpRedirects(httpResponse))

  return checks
}

const reachableWithHttps = (httpsResponse) => {
  return {
    id: '1001',
    title: 'Reachable using HTTPS',
    value: Boolean(httpsResponse),
    severity: 'high',
    category: 'Protocol',
    time: 0
  }
}

const reachableWithHttp = (httpResponse) => {
  return {
    id: '1002',
    title: 'Reachable using HTTP',
    value: Boolean(httpResponse),
    severity: 'low',
    category: 'Protocol',
    time: 0
  }
}

const httpRedirects = (httpResponse) => {
  return {
    id: '1003',
    title: 'HTTP redirects to HTTPS',
    value: httpResponse && httpResponse.responseUrl.startsWith('https://'),
    severity: 'high',
    category: 'Protocol',
    time: 0
  }
}
