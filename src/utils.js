const { http, https } = require('follow-redirects')

module.exports = {
  httpRequest: async (url) => {
    return new Promise((resolve, reject) => {
      const { hostname, port, path } = url
      const options = {
        hostname,
        port,
        path,
        method: 'GET'
      }

      const req = http.request(options, res => {
        resolve(res)
      })

      req.on('error', () => {
        resolve(null)
      })

      req.end()
    })
  },
  httpsRequest: async (url) => {
    return new Promise((resolve, reject) => {
      const { hostname, port, path } = url
      const options = {
        hostname,
        port,
        path,
        method: 'GET'
      }

      const req = https.request(options, res => {
        resolve(res)
      })

      req.on('error', () => {
        resolve(null)
      })

      req.end()
    })
  }
}
