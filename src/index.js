const { URL } = require('url')
const fs = require('fs')
const { httpRequest, httpsRequest } = require('./utils')
const protocolCheck = require('./checks/protocol')
const contentSecurityPolicyCheck = require('./checks/contentSecurityPolicy')
const report = require('./report')

const scanner = async (scanUrl) => {
  const url = new URL(scanUrl)

  const results = []
  const startDate = new Date()

  const httpResponse = await httpRequest(url)
  const httpsResponse = await httpsRequest(url)

  if (!httpResponse && !httpsResponse) {
    console.log('Failed to get a response')
    process.exit(1)
  }

  const response = httpsResponse || httpResponse

  const protocol = await protocolCheck(httpResponse, httpsResponse)
  results.push(...protocol)

  const csp = await contentSecurityPolicyCheck(response)
  results.push(...csp)

  console.log(results)

  const endDate = new Date()
  const time = (endDate.getTime() - startDate.getTime()) / 1000

  fs.writeFileSync('report.xml', report(results, time))
}

const args = process.argv.slice(2)
const urlArg = args[0]

if (urlArg === undefined) {
  console.log('No URI to scan given')
  process.exit(1)
}

scanner(urlArg)
